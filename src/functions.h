#ifndef FUNCTIONS_H_INCLUDED
#define	FUNCTIONS_H_INCLUDED

#include <cmath>

namespace function {

inline
	double zero(const double& x, const double& y) {
	(void)x;
	(void)y;
	return 0;
}

inline
	double one(const double& x, const double& y) {
	(void)x;
	(void)y;
	return 1;
}

inline
double f(const double& x, const double& y) {
	(void) x;
	(void) y;
    return 0;
}

inline
double g(const double& x, const double& y) {
    return sin(M_PI * x) * sinh(M_PI * y);
}

}

#endif
