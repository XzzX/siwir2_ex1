#ifndef MULTIGRID_H_INCLUDED
#define	MULTIGRID_H_INCLUDED

#include	<iostream>
#include	<memory>
#include	<vector>

#include	"grid.h"
#include	"functions.h"

class Multigrid{
private:
	std::vector<std::unique_ptr<Grid>> gridLevels;
	
	inline
	double calcResiduum(Grid& grid);
	inline
	double calcError(Grid& grid);
public:
	Multigrid(const int levels);
	~Multigrid();

	void initFinestGrid();

	inline
		void relax(Grid& grid);

	inline
	double calcResiduum(const int level);
	inline
	double calcError(const int level);

	inline
	void injectToCoarserGrid(Grid& fine, Grid& coarse);
	inline
	void fullWeightingToCoarserGrid(Grid& fine, Grid& coarse);
	inline
	void interpolate(Grid& coarse, Grid& fine);

	inline 
	   double doVCycle(const int iterationsBefore, const int iterationsAfter, const int level = 0);

	inline
		void test();
};

inline
double Multigrid::calcResiduum(Grid& grid){
	//assume hx == hy
	//assume dimensions are quadratic and pow(2,l) + 1
	
	double r2 = 0;
	
	for (int s = 1; s < grid.getNumberOfPointsS() - 1; ++s){
		for (int r = 1; r < grid.getNumberOfPointsR() - 1; ++r){
			grid.getR(r, s) = grid.getF(r, s) - grid.getinvh2() * ( 4 * grid.getU(r, s) - grid.getU(r - 1, s) - grid.getU(r + 1, s) - grid.getU(r, s - 1) - grid.getU(r, s + 1));
			r2 += grid.getR(r, s) * grid.getR(r, s);
		}
	}
	
	return sqrt(r2 / ( (grid.getNumberOfPointsR() - 2) * (grid.getNumberOfPointsS() - 2) ));
}

inline
double Multigrid::calcError(Grid& grid){
	//assume hx == hy
	//assume dimensions are quadratic and pow(2,l) + 1
	
	double e2 = 0;
	
	for (int s = 1; s < grid.getNumberOfPointsS() - 1; ++s){
		for (int r = 1; r < grid.getNumberOfPointsR() - 1; ++r){
			double e = grid.getU(r, s) - function::g(grid.getX(r, s), grid.getY(r, s));
			e2 += e*e;
		}
	}
	
	return sqrt(e2) / ( (grid.getNumberOfPointsR() - 2) * (grid.getNumberOfPointsS() - 2) );
}

inline
void Multigrid::relax(Grid& grid){
	//assume hx == hy
	//assume dimensions are quadratic and pow(2,l) + 1
	
	int width = std::floor(grid.getNumberOfPointsR() * 0.5);

	//#pragma omp parallel for
	for (int b = 1; b < grid.getNumberOfPointsS() - 1; ++b) {
		int offset = ((b + 1) % 2);
		for (int a = offset; a < width; ++a) {
			grid.getuRed(a, b) = 0.25 * (grid.geth2() * grid.getfRed(a, b) + grid.getuBlack(a - offset, b) + grid.getuBlack(a + 1 - offset, b) + grid.getuBlack(a, b - 1) + grid.getuBlack(a, b + 1));
		}
	}

	//#pragma omp parallel for
	for (int b = 1; b < grid.getNumberOfPointsS() - 1; ++b) {
		int offset = (b % 2);
		for (int a = offset; a < width; ++a) {
			grid.getuBlack(a, b) = 0.25 * (grid.geth2() * grid.getfBlack(a, b) + grid.getuRed(a - offset, b) + grid.getuRed(a + 1 - offset, b) + grid.getuRed(a, b - 1) + grid.getuRed(a, b + 1));
		}
	}
}

inline
double Multigrid::calcResiduum(const int level){
	return calcResiduum(*gridLevels[level]);
}

inline
double Multigrid::calcError(const int level){
	return calcError(*gridLevels[level]);
}

inline
void Multigrid::injectToCoarserGrid(Grid& fine, Grid& coarse){
	/*
	double invh2 = 1.0 / (fine.geth() * fine.geth());

	for (int s = 1; s < coarse.getNumberOfPointsS() - 1; ++s){
		for (int r = 1; r < coarse.getNumberOfPointsR() - 1; ++r){
			coarse.getF(r, s) = fine.getF(r * 2, s * 2) - invh2 * ( 4 * fine.getU(r * 2, s * 2) - fine.getU(r * 2 - 1, s * 2) - fine.getU(r * 2 + 1, s * 2) - fine.getU(r * 2, s * 2 - 1) - fine.getU(r * 2, s * 2 + 1));
			coarse.setGrid(function::zero);
		}
	}*/
	calcResiduum(fine);
	for (int s = 1; s < coarse.getNumberOfPointsS() - 1; ++s){
		for (int r = 1; r < coarse.getNumberOfPointsR() - 1; ++r){
			coarse.getF(r, s) = fine.getR(r * 2, s * 2);
			coarse.setGrid(function::zero);
		}
	}
}

inline
void Multigrid::fullWeightingToCoarserGrid(Grid& fine, Grid& coarse){
	calcResiduum(fine);

	for (int s = 1; s < coarse.getNumberOfPointsS() - 1; ++s){
		for (int r = 1; r < coarse.getNumberOfPointsR() - 1; ++r){
			coarse.getF(r, s) = ( 4 * fine.getR(r * 2, s * 2) + 
								  2 * (fine.getR(r * 2 + 1, s * 2) + fine.getR(r * 2 - 1, s * 2) + fine.getR(r * 2, s * 2 + 1) + fine.getR(r * 2, s * 2 - 1)) +
								  1 * (fine.getR(r * 2 + 1, s * 2 + 1) + fine.getR(r * 2 - 1, s * 2 + 1) + fine.getR(r * 2 + 1, s * 2 - 1) + fine.getR(r * 2 - 1, s * 2 - 1)) ) / 16.0;
			coarse.setGrid(function::zero);
		}
	}
}

inline
void Multigrid::interpolate(Grid& coarse, Grid& fine){
	for (int s = 1; s < fine.getNumberOfPointsS() - 1; ++s){
		for (int r = 1; r < fine.getNumberOfPointsR() - 1; ++r){
			fine.getU(r, s) += 0.25 * ( coarse.getU(r / 2, s / 2) + coarse.getU( (r + 1) / 2, s / 2) + coarse.getU(r / 2, (s + 1) / 2) + coarse.getU((r + 1) / 2, (s + 1) / 2) ); 
		}
	}
}

inline 
double Multigrid::doVCycle(const int iterationsBefore, const int iterationsAfter, const int level){
	for (int i = 0; i < iterationsBefore; ++i){
		relax(*gridLevels[level]);
	}

	if (level < gridLevels.size() - 1) {
		fullWeightingToCoarserGrid(*gridLevels[level], *gridLevels[level + 1]);
		doVCycle(iterationsBefore, iterationsAfter, level + 1);
		interpolate(*gridLevels[level + 1], *gridLevels[level]);
	} 

	for (int i = 0; i < iterationsAfter; ++i){
		relax(*gridLevels[level]);
	}
	
	return calcResiduum(*gridLevels[level]);
}

#endif
