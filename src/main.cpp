#include	<stdlib.h>
#include	<cstdio>
#include	<iostream>
#include	<sstream>
#include	<fstream>
#include	<cmath>
#include	<vector>

#include	"Timer.h"

#include	"functions.h"
#include	"grid.h"
#include	"multigrid.h"
#include	"util.h"

int main(int argc, char **argv) {
    ///******************************************************
    ///********************** INPUT *************************
    ///******************************************************

    if (argc != 3) {
		std::cout << "invalid number of arguments!" << std::endl;
		std::cout << "call: mgsolve l n" << std::endl;
        return EXIT_FAILURE;
    }

    int	l = 0;
    int	n = 0;
    l = StringTo<int>(argv[1]);
    n = StringTo<int>(argv[2]);
	
	Multigrid multigrid(l);

    ///******************************************************
    ///********************** CALCULATION *******************
    ///******************************************************

    siwir::Timer	timer;
	
	double r = 0;
	double rtemp = 0;
	
	for (int i = 0; i < n; ++i){
		r = multigrid.doVCycle(2, 1);
		std::cout << "residuum: " << r << "\t convergence: " << r/ rtemp << "\t error: " << multigrid.calcError(0) << std::endl;
		rtemp = r;
	}
	
	std::cout << "error: " << multigrid.calcError(0) << std::endl;
	std::cout << "time: " << timer.elapsed() << std::endl;

    ///******************************************************
    ///********************** OUTPUT ************************
    ///******************************************************

    return EXIT_SUCCESS;
};
